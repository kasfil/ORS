<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Luthier\MiddlewareInterface;

class AuthMiddleware implements MiddlewareInterface {

    public function run($args) {

        // get uri string user whanna go
        $uri = uri_string();

        // check if the user is logged in
        if (! ci()->ion_auth->logged_in()) {
            // if the user not regular entry then put redirect there
            if ($uri != 'admin') {

                $nextPage = urlencode($uri);

                redirect('/admin/login?return='.$nextPage, 'refresh');
            } else {

                redirect('admin/login', 'refresh');
            }
        }
    }
}