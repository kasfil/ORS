<?php

// use Luthier\Route;

/**
 * Welcome to Luthier-CI!
 *
 * This is your main route file. Put all your HTTP-Based routes here using the static
 * Route class methods
 *
 * Examples:
 *
 *    Route::get('foo', 'bar@baz');
 *      -> $route['foo']['GET'] = 'bar/baz';
 *
 *    Route::post('bar', 'baz@fobie', [ 'namespace' => 'cats' ]);
 *      -> $route['bar']['POST'] = 'cats/baz/foobie';
 *
 *    Route::get('blog/{slug}', 'blog@post');
 *      -> $route['blog/(:any)'] = 'blog/post/$1'
 */

Route::match(['GET', 'POST'], '/', 'Register@index')->name('Homepage');

Route::match(['GET', 'POST'], 'admin/login', 'Auth@login')->name('Admin.Login');

Route::group('entry', function(){
    Route::match(['GET', 'POST'], 'datasantri', 'Register@dataSantri');
    Route::match(['GET', 'POST'], 'data/{num:id}/{((ayah|ibu|wali)):state}', 'Register@dataWali');
    Route::match(['GET', 'POST'], 'data/{num:id}/agreement', 'Register@agreement');
    Route::get('data/{num:id}/complete', 'Register@complete');
});

Route::group('admin', ['middleware'=> 'AuthMiddleware'], function(){
    Route::get('/', 'Admin@index')->name('Admin.Home');
    Route::get('data/santri', 'Admin@santriData')->name('Admin.DataEntry');
    Route::get('data/raw/santri/{num:pagenum?}', 'Admin@rawSantriData')->name('Admin.RawDataEntry');
    Route::get('data/{num:id}/detail', 'Admin@studentDetail')->name('Admin.StudentDetail');
    Route::get('users', 'Admin@users')->name('Admin.Users');
    Route::post('user/add', 'Auth@addUser')->name('Admin.addUser');
    Route::get('user/{num:id}/deactivate', 'Auth@deactivate')->name('Admin.DeactivateUser');
    Route::get('user/{num:id}/activate', 'Auth@activate')->name('Admin.ActivateUser');
    Route::get('profile', 'Admin@profile')->name('Admin.Profile');
    Route::post('profile/{num:id}/update', 'Auth@updateProfile')->name('admin.UpdateProfile');
    Route::get('logout', 'Auth@logout')->name('Admin.Logout');
});

// api Route
Route::get('api/latecoupon', 'Data@latecoupon');
Route::get('api/chartdata', 'Data@getChartData');
Route::get('api/lateststudent', 'Data@getLatestStudents');
Route::get('api/quickcoupon', 'Data@quickCoupon');
// Route::get('fakeit/{num:count}', 'Data@fakeit');

Route::set('404_override', function(){
    show_404();
});

Route::set('translate_uri_dashes',FALSE);