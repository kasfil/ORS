DROP TABLE IF EXISTS `groups`;

#
# Table structure for table 'groups'
#

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table 'groups'
#

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
     (1,'admin','Administrator'),
     (2,'members','General User');



DROP TABLE IF EXISTS `users`;

#
# Table structure for table 'users'
#

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `uc_email` UNIQUE (`email`),
  CONSTRAINT `uc_username` UNIQUE (`username`),
  CONSTRAINT `uc_activation_selector` UNIQUE (`activation_selector`),
  CONSTRAINT `uc_forgotten_password_selector` UNIQUE (`forgotten_password_selector`),
  CONSTRAINT `uc_remember_selector` UNIQUE (`remember_selector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Dumping data for table 'users'
#

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_code`, `forgotten_password_code`, `created_on`, `last_login`, `active`, `phone`) VALUES
     ('1','127.0.0.1','administrator','$2y$08$200Z6ZZbp3RAEXoaWcMA6uJOFicwNZaqk4oDhqTUiFXFe63MG.Daa','admin@admin.com','',NULL,'1268889823','1268889823','1', '0');


DROP TABLE IF EXISTS `users_groups`;

#
# Table structure for table 'users_groups'
#

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `uc_users_groups` UNIQUE (`user_id`, `group_id`),
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
     (1,1,1),
     (2,1,2);


DROP TABLE IF EXISTS `login_attempts`;

#
# Table structure for table 'login_attempts'
#

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `coupons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `coupon` varchar(7) NOT NULL,
  `step1` tinyint(1) unsigned DEFAULT 0,
  `step2` tinyint(1) unsigned DEFAULT 0,
  `step3` tinyint(1) unsigned DEFAULT 0,
  `complete_at` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `students` (
  `id` bigint(15) unsigned NOT NULL,
  `coupon_id` int(11) unsigned NOT NULL,
  `nama` varchar(125) NOT NULL,
  `panggilan` varchar(125) NOT NULL,
  `gender` enum('l', 'p') NOT NULL,
  `tempat_lahir` varchar(55) NOT NULL,
  `tgl` tinyint(2) unsigned NOT NULL,
  `bln` tinyint(2) unsigned NOT NULL,
  `thn` smallint(4) unsigned NOT NULL,
  `nisn` varchar(10) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `kewarg` enum('1', '2', '3') NOT NULL DEFAULT '1',
  `agama` enum('1', '2', '3', '4', '5', '6', '7') NOT NULL DEFAULT '1',
  `anak_ke` tinyint(2) NOT NULL,
  `bersaudara` tinyint(2) NOT NULL,
  `tiri` tinyint(2) unsigned NOT NULL,
  `angkat` tinyint(2) unsigned NOT NULL,
  `yatim` enum('0', '1', '2', '3') NOT NULL DEFAULT '0',
  `gol_darah` enum('a', 'b', 'ab', 'o') NOT NULL,
  `tinggi` tinyint(3) unsigned NOT NULL,
  `berat` tinyint(3) unsigned NOT NULL,
  `kelainan` varchar(100) DEFAULT NULL,
  `keb_khusus` tinyint(3) unsigned NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `rt` tinyint(3) unsigned DEFAULT NULL,
  `rw` tinyint(3) unsigned DEFAULT NULL,
  `kota` varchar(125) NOT NULL,
  `prov` varchar(125) NOT NULL,
  `ayah` int(11) unsigned DEFAULT NULL,
  `ibu` int(11) unsigned DEFAULT NULL,
  `wali` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `coupon_used` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `walis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('1', '2', '3', '4', '5', '6', '7') NOT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `tempat_lahir` varchar(55) NOT NULL,
  `tgl` tinyint(2) unsigned NOT NULL,
  `bln` tinyint(2) unsigned NOT NULL,
  `thn` smallint(4) unsigned NOT NULL,
  `agama` tinyint(1) unsigned DEFAULT 1,
  `kewarganegaraan` enum('1', '2', '3') DEFAULT '1',
  `masih_bersama`  tinyint(1) unsigned DEFAULT 1,
  `pendidikan` tinyint(2) unsigned NOT NULL,
  `pekerjaan` tinyint(2) unsigned NOT NULL,
  `penghasilan` tinyint(1) unsigned NOT NULL,
  `pengeluaran` tinyint(1) unsigned NOT NULL,
  `life` tinyint(1) unsigned DEFAULT 1,
  `phone` varchar(17) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `berkas` (
  `id` bigint(15) unsigned NOT NULL,
  `type` enum('ktp_ortu','foto_ortu','ijasah','skhu','kk','akte','prestasi') NOT NULL,
  `filepath` varchar(191) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `berkas_owner` FOREIGN KEY (`id`) REFERENCES `students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `students`
  ADD CONSTRAINT `stundent_to_father` FOREIGN KEY (`ayah`) REFERENCES `walis` (`id`) ON DELETE CASCADE;

ALTER TABLE `students`
  ADD CONSTRAINT `stundent_to_mother` FOREIGN KEY (`ibu`) REFERENCES `walis` (`id`) ON DELETE CASCADE;

ALTER TABLE `students`
  ADD CONSTRAINT `stundent_to_wali` FOREIGN KEY (`wali`) REFERENCES `walis` (`id`) ON DELETE CASCADE;

COMMIT;