<?php

use phpDocumentor\Reflection\Types\Boolean;

class Data extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(['students', 'coupon']);
        $this->load->helper(['coupon']);
    }

    // fetch admin data from ajax
    public function getChartData() {
        
        $data['total']['all'] = $this->db->count_all_results('students');
        $data['total']['male'] = $this->db->where('gender', 'l')->from('students')->count_all_results();
        $data['total']['female'] = $this->db->where('gender', 'p')->from('students')->count_all_results();

        $data['senior']['all'] = $this->db->where('grade', 2)->from('students')->count_all_results();
        $data['senior']['male'] = $this->db->where('grade', 2)->where('gender', 'l')->from('students')->count_all_results();
        $data['senior']['female'] = $this->db->where('grade', 2)->where('gender', 'p')->from('students')->count_all_results();

        $data['junior']['all'] = $this->db->where('grade', 1)->from('students')->count_all_results();
        $data['junior']['male'] = $this->db->where('grade', 1)->where('gender', 'l')->from('students')->count_all_results();
        $data['junior']['female'] = $this->db->where('grade', 1)->where('gender', 'p')->from('students')->count_all_results();


        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function latecoupon(){

        $this->db->order_by('id', 'DESC');
        $this->db->limit(5);
        $latestCoup = $this->db->get('coupons');

        header('Content-type: application/json');
        echo json_encode($latestCoup->result());
    }

    public function getLatestStudents() {

        $this->db->select('coupons.coupon, students.id, students.grade, students.gender, students.nama, students.panggilan, students.anak_ke, students.bersaudara, students.kota');
        $this->db->order_by('baseid', 'DESC');
        $this->db->limit(10);
        $this->db->from('students');
        $this->db->join('coupons', 'coupons.id = students.coupon_id');
        $query = $this->db->get();

        header('Content-type: application/json');
        echo json_encode($query->result());
    }

    public function quickCoupon() {

        do {
            $coupon = generateCoupon();
            $exist = (Boolean)$this->coupon->existCheck($coupon);
        } while ($exist == TRUE);

        $data['coupon'] = $coupon;

        $this->db->insert('coupons', $data);

        header('Content-type: application/json');
        echo json_encode($data);
    }

    // fake student data using faker PHP
    // public function fakeit($count = 1) {
    //     $faker = Faker\Factory::create('id_ID');
    //     $coupons = [];
    //     $data = [];

    //     // for ($i=0; $i < $count; $i++) { 
    //     //     $coupon['coupon'] = generateCoupon();

    //     //     array_push($coupons, $coupon);
    //     // }

    //     // $this->db->insert_batch('coupons', $coupons);

    //     for ($i=0; $i < $count; $i++) {
    //         $student['grade'] = $faker->randomElement([1,2]);
    //         $student['id'] = $this->students->generateId($student['grade']);
    //         $student['gender'] = $faker->randomElement(['l','p']);
    //         $student['nama'] = $faker->name($student['gender'] == 'l' ? 'male' : 'female');
    //         $student['panggilan'] = $faker->firstName($student['gender'] == 'l' ? 'male' : 'female');
    //         $student['coupon_id'] = $i+1;
    //         $student['tempat_lahir'] = $faker->city;
    //         $student['tgl'] = $faker->numberBetween(1, 31);
    //         $student['bln'] = $faker->numberBetween(1, 12);
    //         $student['thn'] = 2003;
    //         $student['nisn'] = $faker->isbn10;
    //         $student['nik'] = $faker->nik();
    //         $student['kewarg'] = 1;
    //         $student['agama'] = 1;
    //         $student['bersaudara'] = $faker->numberBetween(1, 4);
    //         $student['anak_ke'] = $faker->numberBetween(1, $student['bersaudara']);
    //         $student['tiri'] = 0;
    //         $student['angkat'] = 0;
    //         $student['yatim'] = 1;
    //         $student['gol_darah'] = $faker->randomElement(['a', 'b', 'ab', 'o']);
    //         $student['tinggi'] = $faker->numberBetween(123, 180);
    //         $student['berat'] = $faker->numberBetween(40, 72);
    //         $student['keb_khusus'] = 0;
    //         $student['alamat'] = $faker->streetAddress;
    //         $student['rt'] = $faker->numberBetween(1, 7);
    //         $student['rw'] = $faker->numberBetween(1, 7);
    //         $student['desa'] = $faker->streetSuffix;
    //         $student['kecamatan'] = $faker->citySuffix;
    //         $student['kota'] = $faker->city;
    //         $student['prov'] = $faker->state;

    //         $this->db->insert('students', $student);
    //     }

    // }
}