<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Encrypt extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('encryption');
        $this->load->helper('coupon_helper');
    }

    public function index() {
        $key = bin2hex($this->encryption->create_key(32));

        echo $key."\n";

        echo generateCoupon();
    }
}