<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public $coupon;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','translate']);
        $this->load->library(['form_validation','upload']);
        $this->load->model(['coupon', 'students']);
    }

    /**
     * Image requirement upload manager
     *
     * @param string $form
     * @param int $id
     * @param string $name
     * @return void
     */
    public function imageUploadManager($form, $id, $name) {
        // define upload path destination
        $path = APPPATH.'../assets/upload/'.$id;

        // check if path exist
        if (!file_exists($path)) {
            mkdir($path);
        }

        // define configuration for uploading
        $config = [
            'upload_path' => $path,
            'allowed_types' => 'gif|png|jpg|jpeg|bmp',
            'max_size' => 2048,
            'file_name' => $name.'.png'
        ];
        $this->upload->initialize($config);
        $this->upload->do_upload($form);

        $config = [];
    }

    public function index() {
        $rules = [
            [
                'field' => 'coupon',
                'label' => 'Coupon',
                'rules' => 'required|min_length[5]|max_length[7]',
                'errors' => [
                    'required' => 'Kode Pendaftaran tidak boleh kosong',
                    'min_length' => 'Format Kode Pendaftaran salah minimal {param} huruf',
                    'max_length' => 'Format Kode Pendaftaran salah maksimal {param} huruf'
                ]
            ]
        ];
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === FALSE) {

            // common views need
            $data['title'] = 'Pendaftaran';
            $data['css'] = [
                'bulma/bulma.min.css',
                'welcome.css'
            ];

            // get the flash message if there is one
            if ($this->session->flashdata('message')) {
                $data['message'] = $this->session->flashdata('message');
            }

            $this->load->view('_partials/header.php', $data);
            $this->load->view('reg/welcome.php', $data);
            $this->load->view('_partials/footer.php', $data);

        } else {
            // get user coupon
            $coupon = $this->input->post('coupon');

            // check if the coupon exists
            $validCoupon = $this->coupon->existCheck($coupon);

            if (!$validCoupon) {
                $this->session->set_flashdata('message', 'Kode pendaftaran tidak ditemukan, silahkan kontak admin atau cek kembali kode yang diberikan');
                redirect('/', 'refresh');
            } else {
                // Check if they where fill some form
                if (!$validCoupon->step1) {

                    redirect('entry/datasantri?serial='.$validCoupon->coupon, 'refresh');

                } elseif (!$validCoupon->step2) {
                    // Get data coupon's owner
                    $couponOwner = $this->students->specificStudent($validCoupon->id);

                    if ($couponOwner->ayah === NULL) {
                        // Redirect to fill father's form
                        redirect('entry/data/'.$couponOwner->id.'/ayah');
                    } elseif ($couponOwner->ibu === NULL) {
                        // Redirect to fill mother's form
                        redirect('entry/data/'.$couponOwner->id.'/ibu');
                    } else {
                        // Redirect to fill wali's form
                        redirect('entry/data/'.$couponOwner->id.'/wali');
                    }
                } elseif (!$validCoupon->step3) {
                    redirect('entry/data/'.$couponOwner->id.'/confirmation', 'refresh');
                } else {
                    redirect('entry/data/'.$couponOwner->id.'/complete', 'refresh');
                }
            }
        }
    }

    public function dataSantri () {

        if (!$this->input->get('serial')) {
            show_404();
        }

        $coupon = $this->coupon->existCheck($this->input->get('serial'));
        $rules = [
            [
                'field' => 'grade',
                'label' => 'sekolah',
                'rules' => 'required|numeric|in_list[1,2]',
                'errors' => [
                    'in_list' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'nama',
                'label' => 'Nama Lengkap',
                'rules' => 'required|max_length[125]',
                'errors' => [
                    'required' => 'Nama tidak boleh kosong',
                    'max_length' => 'Tidak boleh lebih dari 125 karakter',
                    'regex_match' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'panggilan',
                'label' => 'Nama Panggilan',
                'rules' => 'required|max_length[125]',
                'errors' => [
                    'required' => 'Panggilan tidak boleh kosong',
                    'max_length' => 'Tidak boleh lebih dari 125 karakter',
                    'regex_match' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'gender',
                'label' => 'Jenis Kelamin',
                'rules' => 'required|in_list[l,p]',
                'errors' => [
                    'in_list' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'tmpt_lahir',
                'label' => 'Tempat Lahir',
                'rules' => 'required|max_length[55]',
                'errors' => [
                    'required' => 'Tempat Lahir tidak boleh kosong',
                    'max_length' => 'Tidak boleh lebih dari 55 karakter',
                    'regex_match' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'tgl',
                'label' => 'Tanggal',
                'rules' => 'required|max_length[2]|numeric',
                'errors' => [
                    'required' => 'Tanggal tidak boleh kosong',
                    'max_length' => 'Tidak boleh lebih dari 2 karakter',
                    'numeric' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'bln',
                'label' => 'Bulan',
                'rules' => 'required|max_length[2]|numeric',
                'errors' => [
                    'required' => 'Bulan tidak boleh kosong',
                    'max_length' => 'Tidak boleh lebih dari 2 karakter',
                    'numeric' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'thn',
                'label' => 'Tahun',
                'rules' => 'required|max_length[4]|numeric',
                'errors' => [
                    'required' => 'Tanggal tidak boleh kosong',
                    'max_length' => 'Tidak boleh lebih dari 4 karakter',
                    'numeric' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'nisn',
                'label' => 'NISN',
                'rules' => 'required|max_length[11]|numeric',
                'errors' => [
                    'required' => 'NISN tidak boleh kosong',
                    'max_length' => 'Tidak boleh lebih dari 11 karakter',
                    'numeric' => 'NISN Hanya Terdiri dari angka'
                ]
            ],
            [
                'field' => 'nik',
                'label' => 'NIK',
                'rules' => 'required|max_length[17]|numeric',
                'errors' => [
                    'required' => 'NISN tidak boleh kosong',
                    'max_length' => 'Tidak boleh lebih dari 17 karakter',
                    'numeric' => 'NIK Hanya Terdiri dari angka'
                ]
            ],
            [
                'field' => 'kewarg',
                'label' => 'Kewarganegaraan',
                'rules' => 'required|in_list[1,2,3]',
                'errors' => [
                    'in_list' => 'Data tidak sesuai'
                ]
            ],
            [
                'field' => 'agama',
                'label' => 'Agama',
                'rules' => 'required|less_than[9]|greater_than[0]'
            ],
            [
                'field' => 'anak_ke',
                'label' => 'Anak Ke',
                'rules' => 'required|greater_than[0]|max_length[2]|numeric'
            ],
            [
                'field' => 'bersaudara',
                'label' => 'Bersaudara',
                'rules' => 'required|max_length[2]|numeric'
            ],
            [
                'field' => 'tiri',
                'label' => 'angkat',
                'rules' => 'required|max_length[2]|numeric'
            ],
            [
                'field' => 'angkat',
                'label' => 'Angkat',
                'rules' => 'required|max_length[2]|numeric'
            ],
            [
                'field' => 'yatim',
                'label' => 'Yatim',
                'rules' => 'required|in_list[0,1,2,3]'
            ],
            [
                'field' => 'gol_darah',
                'label' => 'Golongan Darah',
                'rules' => 'required|in_list[a,b,ab,o]'
            ],
            [
                'field' => 'tinggi',
                'label' => 'Tinggi',
                'rules' => 'required|less_than[300]|greater_than[15]'
            ],
            [
                'field' => 'berat',
                'label' => 'Berat',
                'rules' => 'required|less_than[150]|greater_than[15]'
            ],
            [
                'field' => 'keb_khusus',
                'label' => 'Kebutuhan Khusus',
                'rules' => 'required|less_than[23]|greater_than[-1]'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required|max_length[255]',
            ],
            [
                'field' => 'rt',
                'label' => 'RT',
                'rules' => 'required|numeric|max_length[2]',
                'errors' => [
                    'required' => 'RT tidak boleh kosong'
                ]
            ],
            [
                'field' => 'rw',
                'label' => 'RW',
                'rules' => 'required|numeric|max_length[2]',
                'errors' => [
                    'required' => 'RT tidak boleh kosong'
                ]
            ],
            [
                'field' => 'desa',
                'label' => 'Desa',
                'rules' => 'required|max_length[125]',
                'errors' => [
                    'required' => 'Desa tidak boleh kosong'
                ]
            ],
            [
                'field' => 'kecamatan',
                'label' => 'Kecamatan',
                'rules' => 'required|max_length[125]',
                'errors' => [
                    'required' => 'Kecamatan tidak boleh kosong'
                ]
            ],
            [
                'field' => 'kota',
                'label' => 'Kota',
                'rules' => 'required|max_length[125]',
                'errors' => [
                    'required' => 'Kota tidak boleh kosong'
                ]
            ],
            [
                'field' => 'prov',
                'label' => 'Provinsi',
                'rules' => 'required|max_length[125]',
                'errors' => [
                    'required' => 'Provinsi tidak boleh kosong'
                ]
            ]
        ];

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === TRUE) {

            // if user input is valid

            // define variable array to catch all user input
            // then assign to column name in database
            $data = [
                'coupon_id' => $coupon->id,
                'grade' => $this->input->post('grade'),
                'nama' => $this->input->post('nama'),
                'panggilan' => $this->input->post('panggilan'),
                'gender' => $this->input->post('gender'),
                'tempat_lahir' => $this->input->post('tmpt_lahir'),
                'tgl' => $this->input->post('tgl'),
                'bln' => $this->input->post('bln'),
                'thn' => $this->input->post('thn'),
                'kewarg' => $this->input->post('kewarg'),
                'agama' => $this->input->post('agama'),
                'anak_ke' => $this->input->post('anak_ke'),
                'bersaudara' => $this->input->post('bersaudara'),
                'tiri' => $this->input->post('tiri'),
                'angkat' => $this->input->post('angkat'),
                'yatim' => $this->input->post('yatim'),
                'nisn' => $this->input->post('nisn'),
                'nik' => $this->input->post('nik'),
                'tinggi' => $this->input->post('tinggi'),
                'berat' => $this->input->post('berat'),
                'gol_darah' => $this->input->post('gol_darah'),
                'keb_khusus' => $this->input->post('keb_khusus'),
                'prov' => $this->input->post('prov'),
                'kota' => $this->input->post('kota'),
                'kecamatan' => $this->input->post('kecamatan'),
                'desa' => $this->input->post('desa'),
                'alamat' => $this->input->post('alamat'),
                'rt' => $this->input->post('rt'),
                'rw' => $this->input->post('rw')
            ];

            $data['id'] = $this->students->generateId($data['grade']);

            if ($this->db->insert('students', $data)) {

                // set info that this coupon has passed step1
                $this->coupon->updateStep($data['coupon_id'], 'step1');

                // get all user upload provided if there is some
                if (!empty('ijasah')) {
                    $this->imageUploadManager('ijasah', $data['id'], 'ijasah');
                }

                if (!empty('skhu')) {
                    $this->imageUploadManager('skhu', $data['id'], 'skhu');
                }

                if (!empty('akta')) {
                    $this->imageUploadManager('akta', $data['id'], 'akta');
                }

                if (!empty('kk')) {
                    $this->imageUploadManager('kk', $data['id'], 'kk');
                }

                redirect('entry/data/'.$data['id'].'/ayah', 'refresh');
            } else {
                $this->session->set_flashdata('problem', 'Data gagal masuk ke database');
                $this->session->set_flashdata('solution', 'Mohon hubungi admin pembenahan masalah');

                redirect(route('universal_error'), 'refresh');
            }
            
        } else {

            $data = [
                'title' => 'Entry Data Pendaftaran',
                'css' => [
                    'bulma/bulma.min.css',
                    'bulma/bulma-steps.css',
                    'reg.css'
                ],
                'footJS' => [
                    'form-suggest.js',
                    'upload-file-name.js'
                ],
                'stateCount' => 1,
                'currentState' => 'Entry Data Santri',
                'coupon' => $coupon->coupon,
                'kebKhusus' => [
                    'Tidak',
                    'Tuna netra',
                    'Tuna rungu',
                    'Tuna wicara',
                    'Tuna laras',
                    'Tuna ganda',
                    'Grahita ringan',
                    'Grahita sedang',
                    'Daksa ringan',
                    'Daksa sedang',
                    'Hyperaktif',
                    'Cerdas istimewa',
                    'Bakat istimewa',
                    'Kesulitan belajar',
                    'Narkoba',
                    'Indigo',
                    'Down syndrome',
                    'Autis',
                    'Terbelakang',
                    'Bencana Alam/Sosial',
                    'Tidak Mampu Ekonomi'
                ]
            ];

            $this->load->view('_partials/header.php', $data);
            $this->load->view('_partials/regnav.php', $data);
            $this->load->view('_partials/regSteps.php', $data);
            $this->load->view('reg/students.php', $data);
            $this->load->view('_partials/adminFooter.php', $data);
            $this->load->view('_partials/footer.php', $data);

        }
    }

    public function dataWali($student_id, $state) {

        $data = [
            'title' => 'Entry Data Pendaftaran',
            'css' => [
                'bulma/bulma.min.css',
                'bulma/bulma-steps.css',
                'reg.css'
            ],
            'footJS' => [
                'upload-file-name.js',
                'hide-form.js'
            ],
            'student_id' => $student_id,
            'state' => $state,
            'currentState' => 'Entry Data '.$state,
            'newData' => TRUE,
            'relation' => $this->input->post('type') != NULL ? $this->input->post('type') : NULL,
            'pendidikan' => [
                'Tidak Sekolah',
                'Putus SD',
                'SD Sederajat',
                'SMP Sederajat',
                'SMA Sederajat',
                'D1',
                'D2',
                'D3',
                'D4/S1',
                'S2',
                'S3'
            ],
            'pekerjaan' => [
                'Tidak Bekerja',
                'Nelayan',
                'Petani',
                'Peternak',
                'PNS',
                'Guru / Dosen',
                'TNI / Polri',
                'Karyawan Swasta',
                'Pedagang Kecil',
                'Pedagang Besar',
                'Wiraswasta',
                'Wirausaha',
                'Buruh',
                'Pensiunan',
                'TKI',
                'lainnya'
            ]
        ];

        // get student parents info
        $student = $this->students->specificStudent($student_id, 'id');

        $rules = [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required|max_length[255]',
                'errors' => [
                    'required' => 'Nama tidak boleh kosong',
                    'maxx_length' => 'Nama tidak lebih dari 255 karakter'
                ]
            ],
            [
                'field' => 'tempat_lahir',
                'label' => 'Tempat Lahir',
                'rules' => 'required|max_length[55]',
                'errors' => [
                    'required' => '{label} tidak boleh kosong',
                    'max_length' => '{label} tidak boleh lebih dari 55 karakter'
                ]
            ],
            [
                'field' => 'tgl',
                'label' => 'Tanggal',
                'rules' => 'required|numeric|max_length[2]',
                'errors' => [
                    'required' => '{label} tidak boleh kosong',
                    'numeric' => '{label} hanya berupa angka',
                    'max_length' => '{label} tidak boleh lebih dari {param} karakter'
                ]
            ],
            [
                'field' => 'bln',
                'label' => 'Bulan',
                'rules' => 'required|numeric|max_length[2]',
                'errors' => [
                    'required' => '{label} tidak boleh kosong',
                    'numeric' => '{label} hanya berupa angka',
                    'max_length' => '{label} tidak boleh lebih dari {param} karakter'
                ]
            ],
            [
                'field' => 'thn',
                'label' => 'Tahun',
                'rules' => 'required|numeric|max_length[4]',
                'errors' => [
                    'required' => '{label} tidak boleh kosong',
                    'numeric' => '{label} hanya berupa angka',
                    'max_length' => '{label} tidak boleh lebih dari {param} karakter'
                ]
            ],
            [
                'field' => 'agama',
                'label' => 'Agama',
                'rules' => 'required|numeric',
            ],
            [
                'field' => 'kewarg',
                'label' => 'Kewarganegaraan',
                'rules' => 'required|in_list[1,2,3]'
            ],
            [
                'field' => 'masih_bersama',
                'label' => 'Masih Bersama',
                'rules' => 'required|in_list[1,0]'
            ],
            [
                'field' => 'pendidikan',
                'label' => 'Pendidikan',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'pekerjaan',
                'label' => 'Pekerjaan',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'penghasilan',
                'label' => 'Penghasilan',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'pengeluaran',
                'label' => 'Pengeluaran',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'life',
                'label' => 'Masih Hidup',
                'rules' => 'required|in_list[1,0]'
            ],
            [
                'field' => 'phone',
                'label' => 'Telepon',
                'rules' => 'numeric|max_length[15]'
            ]
        ];

        if ($state == 'ayah') {
            $data['stateCount'] = 2;
            $data['relation'] = 1;
        } elseif ($state == 'ibu') {
            $data['stateCount'] = 3;
            $data['relation'] = 2;
        } else {
            $data['stateCount'] = 4;
            if ($data['relation'] == 1 || $data['relation'] == 2) {
                $data['newData'] = FALSE;
            }
        }
        
        if ($data['newData']) {
            
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === FALSE) {

                // load views
                $this->load->view('_partials/header.php', $data);
                $this->load->view('_partials/regnav.php', $data);
                $this->load->view('_partials/regSteps.php', $data);
                $this->load->view('reg/walis.php', $data);
                $this->load->view('_partials/adminFooter.php', $data);
                $this->load->view('_partials/footer.php', $data);
            } else {
                // prepare variable for inserting wali's data
                $insertData = [];
                // get all user input and put it into array keys
                // according to its apropiate column
                for ($i=0; $i < count($rules); $i++) { 
                    $insertData[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
                }

                // Define wali type
                $insertData['type'] = $data['relation'];
                // check the user input phone if it prefixed wit 0
                // trim it and chang the value
                $insertData['phone'] = substr($insertData['phone'], 0, 1) == 0 ? substr($insertData['phone'], 1) : $insertData['phone'];
                // insert data to database
                $this->db->insert('walis', $insertData);
                // get the id of last input and asign to $lastId
                $lastId = $this->db->insert_id();
                // insert the $lastId to students owner
                $this->students->insertParent($student_id, $lastId, $state);
                // upload file if it not null
                // check if foto file is not empty
                if (!empty('foto')) {
                    $this->imageUploadManager('foto', $student_id, 'foto_'.$state);
                }
                // check if ktp file is not empty
                if (!empty('ktp')) {
                    $this->imageUploadManager('ktp', $student_id, 'ktp_'.$state);
                }

                // input data complete then redirect to
                // the next page according to it's state
                if ($state == 'ayah') {
                    redirect('entry/data/'.$student_id.'/ibu', 'refresh');
                } elseif ($state == 'ibu') {
                    redirect('entry/data/'.$student_id.'/wali', 'refresh');
                } else {
                    $this->coupon->updateStep($student->coupon_id, 'step2');
                    redirect('entry/data/'.$student_id.'/agreement', 'refresh');
                }
            }
        } else {
            // assign value to column wali if data exist
            if ($data['relation'] == 1) {
                $this->students->insertParent($data['student_id'], $student->ayah, 'wali');
            } else {
                $this->students->insertParent($data['student_id'], $student->ibu, 'wali');
            }
            $this->coupon->updateStep($student->coupon_id, 'step2');
            redirect('entry/data/'.$student_id.'/agreement', 'refresh');
        }
    }

    public function agreement($student_id) {
        $data = [
            'title' => 'Entry Data Pendaftaran',
            'css' => [
                'bulma/bulma.min.css',
                'bulma/bulma-steps.css',
                'reg.css'
            ],
            'student_id' => $student_id,
            'stateCount' => 5,
            'currentState' => 'Persetujuan & Peryataan',
        ];

        $rules = [
            [
                'field' => 'pernyataan',
                'label' => 'Pernyataan',
                'rules' => 'required|in_list[1]',
                'errors' => [
                    'required' => 'Anda harus mengetahui dan setuju sebelum melanjutkan',
                    'in_list' => 'Anda harus mengetahui dan setuju sebelum melanjutkan'
                ]
            ]
        ];

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === FALSE) {

            // load views
            $this->load->view('_partials/header.php', $data);
            $this->load->view('_partials/regnav.php', $data);
            $this->load->view('_partials/regSteps.php', $data);
            $this->load->view('reg/agreement.php', $data);
            $this->load->view('_partials/adminFooter.php', $data);
            $this->load->view('_partials/footer.php', $data);
        } else {
            // get student data according to his id
            $student = $this->students->specificStudent($student_id, 'id');
            // take coupon_id column for coupon update step function
            $coupon_id = $student->coupon_id;
            // update step info for coupon
            $this->coupon->updateStep($student->coupon_id, 'step3');
            // redirect to next page (end then thank you)
            redirect('entry/data/'.$data['student_id'].'/complete', 'refresh');
        }
    }

    public function complete($student_id) {
        $data = [
            'title' => 'Entry Data Pendaftaran',
            'css' => [
                'bulma/bulma.min.css',
                'bulma/bulma-steps.css',
                'bulma/bulma-timeline.min.css',
                'reg.css'
            ],
            'student_id' => $student_id,
            'stateCount' => 6,
            'currentState' => 'Terima Kasih',
        ];

        $this->load->view('_partials/header.php', $data);
        $this->load->view('_partials/regnav.php', $data);
        $this->load->view('_partials/regSteps.php', $data);
        $this->load->view('reg/complete.php', $data);
        $this->load->view('_partials/adminFooter.php', $data);
        $this->load->view('_partials/footer.php', $data);
    }
}
