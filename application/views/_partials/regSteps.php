<section class="section is-hidden-mobile">
	<ul class="steps is-narrow is-centered has-content-centered has-gaps">
		<li class="steps-segment <?= $stateCount == 1 ? 'is-active' : '' ?>">
			<a href="#" class="has-text-dark">
				<span class="steps-marker">
                    1
				</span>
				<div class="steps-content">
					<p class="heading">Data Santri</p>
				</div>
			</a>
		</li>
		<li class="steps-segment <?= $stateCount == 2 ? 'is-active' : '' ?>">
			<a href="#" class="has-text-dark">
				<span class="steps-marker">
                    2
				</span>
				<div class="steps-content">
					<p class="heading">Data Ayah</p>
				</div>
			</a>
		</li>
		<li class="steps-segment <?= $stateCount == 3 ? 'is-active' : '' ?>">
			<span class="steps-marker">
                3
			</span>
			<div class="steps-content">
				<p class="heading">Data Ibu</p>
			</div>
		</li>
		<li class="steps-segment <?= $stateCount == 4 ? 'is-active' : '' ?>">
			<span class="steps-marker">
                4
			</span>
			<div class="steps-content">
				<p class="heading">Data Wali</p>
			</div>
        </li>
        <li class="steps-segment <?= $stateCount == 5 ? 'is-active' : '' ?>">
			<span class="steps-marker">
                5
			</span>
			<div class="steps-content">
				<p class="heading">Pernyataan</p>
			</div>
        </li>
        <li class="steps-segment <?= $stateCount == 6 ? 'is-active' : '' ?>">
			<span class="steps-marker">
                6
			</span>
			<div class="steps-content">
				<p class="heading">Tanggal Penting</p>
			</div>
		</li>
	</ul>
</section>
<section class="section is-hidden-tablet">
    <div class="container is-fluid has-text-centered">
        <p class="is-size-6">
            Langkah ke
        </p>
        <div class="circle">
            <p class="state-count is-size-3">
                <?= $stateCount ?>
            </p>
        </div>
        <p class="is-size-6">
            Dari 6 langkah Pendaftaran
        </p>
        <h1 class="is-size-4 is-capitalized">
            <?= $currentState ?>
        </h1>
    </div>
</section>
