    <div class="main-content">
        <div class="container is-fluid">
            <div class="content">
                <p class="mobile-center is-size-5 has-text-dark" style="line-height:36px">
                    <?= $studentTotal ?> calon santri baru, <?= count($students) ?> data yang lengkap <a class="mobile-float-center button is-info" href="<?=route('Admin.RawDataEntry')?>">Lihat Semua Data</a>
                </p>
            </div>
            <section class="section">
                <table class="table is-striped is-fullwidth overflowable">
                    <thead>
                        <tr>
                            <th>Nama Lengkap</th>
                            <th>Sekolah</th>
                            <th>Jenis Kelamin</th>
                            <th>Kota Lahir</th>
                            <th>Tgl Lahir</th>
                            <th>NISN</th>
                            <th>Yatim</th>
                            <th>Keb. Khusus</th>
                            <th>Ayah</th>
                            <th>Ibu</th>
                            <th>Wali</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($students as $student): ?>
                        <tr>
                            <td><a href="<?= base_url()."admin/data/".$student->id."/detail"?>" class="inline-link"><?= $student->nama ?></a></td>
                            <td><?= valueTranslate('grade',$student->grade) ?></td>
                            <td><?= valueTranslate('gender', $student->gender) ?></td>
                            <td><?= $student->tempat_lahir ?></td>
                            <td><?= $student->tgl ?> <?= valueTranslate('bln',$student->bln) ?> <?= $student->thn ?></td>
                            <td><?= $student->nisn ?></td>
                            <td><?= valueTranslate('yatim',$student->yatim) ?></td>
                            <td><?= valueTranslate('keb_khusus',$student->keb_khusus) ?></td>
                            <td><?= $student->ayah ?></td>
                            <td><?= $student->ibu ?></td>
                            <td><?= $student->wali ?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </section>
        </div>
    </div>
