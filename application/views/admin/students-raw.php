    <div class="main-content">
        <div class="container is-fluid">
            <section class="section">
                <table class="table is-striped is-fullwidth overflowable">
                    <thead>
                        <tr>
                            <th class="has-text-centered">Id</th>
                            <th>Kode Pendaftaran</th>
                            <th class="has-text-centered">Nama</th>
                            <th>Sekolah</th>
                            <th>Data Ayah</th>
                            <th>Data Ibu</th>
                            <th>Data Wali</th>
                            <th>Pernyataan</th>
                            <th class="has-text-centered">Selesai Pada</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($students as $student): ?>
                        <tr>
                            <td><?= $student->id ?></td>
                            <td class="has-text-centered"><?= $student->coupon ?></td>
                            <td><?= $student->nama ?></td>
                            <td class="has-text-centered"><?= valueTranslate('grade',$student->grade) ?></td>
                            <td class="has-text-centered"><?= $student->ayah != NULL ? "<i class=\"icon-ok has-text-success\"></i>" : "<i class=\"icon-cancel has-text-danger\"></i>" ?></td>
                            <td class="has-text-centered"><?= $student->ibu != NULL ? "<i class=\"icon-ok has-text-success\"></i>" : "<i class=\"icon-cancel has-text-danger\"></i>" ?></td>
                            <td class="has-text-centered"><?= $student->wali != NULL ? "<i class=\"icon-ok has-text-success\"></i>" : "<i class=\"icon-cancel has-text-danger\"></i>" ?></td>
                            <td class="has-text-centered"><?= $student->tos != '0' ? "<i class=\"icon-ok has-text-success\"></i>" : "<i class=\"icon-cancel has-text-danger\"></i>" ?></td>
                            <td class="has-text-centered"><?= $student->complete_at != NULL ? date('d M Y', strtotime($student->complete_at)) : "<i class=\"icon-cancel has-text-danger\"></i>" ?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
                <nav class="pagination is-centered" role="pagination">
                    <?= $paginate ?>
                </nav>
            </section>
        </div>
    </div>
