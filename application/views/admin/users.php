<section class="section">
    <div class="container">
        <div class="columns">
            <div class="form-wrapper column is-8 is-offset-2">
                <h3 class="is-size-4 has-text-centered is-hidden-mobile">Semua User</h3>
                <section class="section">
                    <table class="table is-striped is-fullwidth">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th class="has-text-centered">Active?</th>
                                <th class="has-text-centered">Last Login</th>
                                <th class="has-text-centered">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($i=0; $i < count($users); $i++): ?>
                            <tr>
                                <td><?= $users[$i]['username'] ?></td>
                                <td><?= $users[$i]['email'] != NULL ? $users[$i]['email'] : 'Tidak Tersedia' ?></td>
                                <td class="has-text-centered"><?= $users[$i]['active'] ? "<i class=\"icon-ok has-text-success\"></i>" : "<i class=\"icon-cancel has-text-danger\"></i>" ?></td>
                                <td class="has-text-centered"><?= $users[$i]['last_login'] !== NULL ? date('d M Y H:i', $users[$i]['last_login']) : "Belum Login" ?></td>
                                <td class="has-text-centered">
                                    <?php if($users[$i]['active']): ?>
                                    <a class="has-text-danger" href="<?= base_url().'admin/user/'.$users[$i]['id'].'/deactivate' ?>">Deactivate</a>
                                    <?php else: ?>
                                    <a class="has-text-success" href="<?= base_url().'admin/user/'.$users[$i]['id'].'/activate' ?>">Activate</a>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <?php endfor; ?>
                        </tbody>
                    </table>
                </section>
                <section class="section">
                    <a class="button is-info is-pulled-right" id="addUser">Tambah User</a>
                </section>
                <div class="modal" id="addUserModal">
                    <div class="modal-background"></div>
                    <div class="modal-content has-background-white-ter" style="max-width: 35%;">
                        <section class="section">
                            <h3 class="is-size-4 has-text-centered">Add User</h3>
                            <?= form_open(base_url().'admin/user/add') ?>
                            <div class="field">
                                <label class="label">Username</label>
                                <div class="control">
                                    <input class="input" name="username" type="text" placeholder="Username">
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Email</label>
                                <div class="control">
                                    <input class="input" name="email" type="text" placeholder="Email">
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Password</label>
                                <div class="control">
                                    <input class="input" name="password" type="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Group</label>
                                <div class="control">
                                    <div class="select is-multiple">
                                        <select name="groups[]" class="is-expanded" style="width:100%" size="2" multiple>
                                            <?php for ($i=0; $i < count($groups); $i++): ?>
                                            <option value="<?= $groups[$i]['id'] ?>"><?= $groups[$i]['name'] ?></option>
                                            <?php endfor ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <input class="button is-link is-pulled-right" type="submit" value="Tambah">
                            </div>
                            <div class="spacer"></div>
                            <?= form_close() ?>
                        </section>
                    </div>
                    <button class="modal-close is-large" id="modalClose" aria-label="close"></button>
                </div>
            </div>
        </div>
    </div>
</section>