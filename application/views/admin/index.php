    <div class="main-content">
        <div class="container is-fluid">
            <div class="chartwrapper">
                <div class="columns is-gapless">
                    <div class="column is-3 has-border-right">
                        <div class="card rounded chart">
                            <header class="card-header has-text-centered">
                                <p class="card-header-title">
                                    <span class="red" id="regTotal"></span> Calon Santri Baru
                                </p>
                            </header>
                            <div class="content">
                                <canvas height="250px" id="pieChart"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="column is-6 has-border-right">
                        <div class="card rounded chart">
                            <header class="card-header has-text-centered">
                                <p class="card-header-title">
                                    Data Santri baru berdasarkan sekolah
                                </p>
                            </header>
                            <div class="content">
                                <canvas height="125px" id="barChart"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="card rounded chart">
                            <header class="card-header has-text-centered">
                                <p class="card-header-title">
                                    Kode Pendaftaran Terbaru
                                </p>
                            </header>
                            <div class="content xpad-7">
                                <table class="table has-text-centered homepage">
                                    <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th class="has-text-centered">santri</th>
                                            <th class="has-text-centered">wali</th>
                                            <th class="has-text-centered">berkas</th>
                                        </tr>
                                    </thead>
                                    <tbody id="couponTable"></tbody>
                                </table>
                                <a class="button is-info homepage" id="newCoupon">Buat Baru</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal" id="couponModal">
                    <div class="modal-background"></div>
                    <div class="modal-content">
                        <div class="card rounded coupon-modal">
                            <header class="card-header has-text-centered">
                                <p class="card-header-title">
                                    Kupon barumu adalah
                                </p>
                            </header>
                            <div class="content has-text-centered">
                                <h1 class="coupon" id="couponOutput"></h1>
                            </div>
                        </div>
                    </div>
                    <button class="modal-close is-large" aria-label="close" id="modalClose"></button>
                </div>
                <div class="spacer"></div>
                <div class="table-wrapper">
                    <table class="table is-striped is-fullwidth">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Kode</th>
                                <th>sekolah</th>
                                <th>Nama</th>
                                <th>Panggilan</th>
                                <th>Jenis Kelamin</th>
                                <th>Anak ke</th>
                                <th>Bersaudara</th>
                                <th>Kota</th>
                            </tr>
                        </thead>
                        <tbody id="studentTable"></tbody>
                    </table>
                </div>
                <div class="spacer"></div>
            </div>
        </div>
    </div>
