<section class="section">
    <div class="container">
        <div class="columns">
            <div class="form-wrapper column is-8 is-offset-2">
                <h3 class="is-size-4 has-text-centered is-hidden-mobile">My Profile</h3>
                <section class="section">
                    <?= form_open(base_url().'admin/profile/'.$id.'/update') ?>
                    <div class="field">
                        <label class="label">Username</label>
                        <div class="control">
                            <input class="input" name="username" type="text" value="<?= $username ?>" placeholder="Username">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Email</label>
                        <div class="control">
                            <input class="input" name="email" value="<?= $email ?>" type="text" placeholder="Email">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Password (Jika ingin merubah password)</label>
                        <div class="control">
                            <input class="input" name="password" type="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="field">
                        <input class="button is-link is-pulled-right" type="submit" value="Update">
                    </div>
                    <div class="spacer"></div>
                    <?= form_close() ?>
                </section>
            </div>
        </div>
    </div>
</section>