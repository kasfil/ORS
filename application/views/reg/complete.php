<section class="section">
	<section class="container">
		<div class="columns">
			<div class="form-wrapper column is-8 is-offset-2">
				<h3 class="is-size-4 has-text-centered is-hidden-mobile">Terima Kasih</h3>
				<p class="is-size-6 has-text-centered">telah mengisi format yang ada</p>
				<section class="section in-form">
					<h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Berikut tanggal
						penting yang perlu diketahui</h3>
					<div class="content">
						<div class="timeline is-centered">
							<div class="timeline-item">
								<div class="timeline-marker is-primary has-text-white has-text-weight-semibold is-icon">
									1
								</div>
								<div class="timeline-content">
									<p class="heading">12 Mei 2019</p>
									<p><strong class="has-text-link">Batas Pendaftaran</strong> Mengisi format online atau format yang telah
										disediakan dioleh panitia</p>
								</div>
							</div>
							<div class="timeline-item">
								<div class="timeline-marker is-primary has-text-white has-text-weight-semibold is-icon">
									2
								</div>
								<div class="timeline-content">
									<p class="heading">13 Mei 2019</p>
									<p><strong class="has-text-link">Tes Seleksi Masuk</strong> wajib memakai pakaian <strong>Hitam Putih</strong>
										dan jilbab hitam bagi putri</p>
									<p><strong>Ujian lisan berupa:</strong> Membaca Alquran, bacaan sholat, Interview / wawancara</p>
									<p><strong>Ujian tulis / psikotes berupa:</strong> Tes IQ dan Berhitung</p>
								</div>
                            </div>
                            <div class="timeline-item">
								<div class="timeline-marker is-primary has-text-white has-text-weight-semibold is-icon">
									3
								</div>
								<div class="timeline-content">
									<p class="heading">14 Mei 2019</p>
									<p><strong class="has-text-link">Pengumuman Hasil Tes</strong> bagi santri yang lulus Tes Ujian Masuk bisa melanjutkan ke tahap selanjutnya (Daftar Ulang)</p>
								</div>
                            </div>
                            <div class="timeline-item">
								<div class="timeline-marker is-primary has-text-white has-text-weight-semibold is-icon">
									4
								</div>
								<div class="timeline-content">
									<p class="heading">15 - 31 Mei 2019</p>
									<p><strong class="has-text-link">Daftar Ulang</strong> serta Menyelesaikan administrasi santri baru</p>
								</div>
                            </div>
                            <div class="timeline-item">
								<div class="timeline-marker is-primary has-text-white has-text-weight-semibold is-icon">
									5
								</div>
								<div class="timeline-content">
									<p class="heading">24 Juni 2019</p>
									<p><strong class="has-text-link">Santri baru masuk asrama</strong></p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section in-form">
					<div class="field is-grouped is-grouped-centered">
						<div class="control">
							<a href="<?= route('Homepage') ?>" class="button is-primary" id="download-button">Ke Halaman Depan</a>
						</div>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
