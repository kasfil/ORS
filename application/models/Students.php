<?php

class Students extends CI_Model {

    /**
     * Generate id for student entry
     *
     * @param int $grade
     * @return int $id
     */ 
    public function generateId($grade) {
        
        $this->db->where('grade', $grade);
        $this->db->from('students');
        $count = $this->db->count_all_results();
        $count++;

        $id = '3'; // base id
        $id .= substr((string)date("Y"), 1); // add 3 last digit of current year
        $id .= '0608'; // add the date and month entry
        $id .= (string)$grade; // add grade initial code 1 for junior 2 for senior
        $id .= sprintf("%'.03d", $count); // get the total student type grade then prepend with 0

        return (int)$id;
    }

    /**
     * Get All students data entry
     * Include all parents
     *
     * @return array $students
     */
    public function getAllCompleteStudents() {
        $this->db->select('students.tempat_lahir, students.tgl, students.bln, students.thn, students.nisn, students.yatim');
        $this->db->select('students.id, students.nama, students.grade, students.gender, students.keb_khusus, ayah.nama as ayah, ibu.nama as ibu, wali.nama as wali');
        $this->db->from('students');
        $this->db->join('walis as ayah', 'students.ayah = ayah.id');
        $this->db->join('walis as ibu', 'students.ibu = ibu.id');
        $this->db->join('walis as wali', 'students.wali = wali.id');
        $students = $this->db->get();

        return $students->result();
    }

    /**
     * get parent status is defined or not
     * in students from the column given
     *
     * @param int $coupon_id
     * @return $query->row();
     */
    public function specificStudent($value, $column = 'coupon_id') {

        $this->db->select('id, coupon_id, ayah, ibu, wali')
                 ->from('students')
                 ->where($column, $value);

        $query = $this->db->get();

        return $query->row();
    }

    /**
     * insert parent's id to it's child
     *
     * @param int $student_id
     * @param int $id
     * @param string $column
     * @return void
     */
    public function insertParent($student_id, $id, $column) {
        $this->db->where('id', $student_id);
        $this->db->set($column, $id);
        $this->db->update('students');
    }

    /**
     * Get raw data from who wanna register
     * get info of their step and data completeness
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getRawStudents($limit, $pageNum) {

        if ($pageNum == 1) {
            $offset = 0;
        } else {
            $offset = ($pageNum - 1) * $limit - 1;
        }

        $this->db->select('students.id, students.nama, students.grade, students.ayah, students.ibu, students.wali')
                 ->select('coupons.coupon, coupons.step3 as tos, coupons.complete_at')
                 ->order_by('baseid', 'DESC')
                 ->from('students')
                 ->limit($limit, $offset)
                 ->join('coupons', 'students.coupon_id = coupons.id');

        $outcome = $this->db->get();

        $query['row'] = $outcome->result();
        $query['total_count'] = $this->db->count_all('students');

        return $query;
    }

    public function getStudentWithId($id) {
        $this->db->select('students.id, students.grade, students.nama, students.panggilan, students.gender, students.tempat_lahir, students.tgl, students.bln, students.thn, students.nisn, students.nik, students.kewarg, students.agama, students.anak_ke, students.bersaudara, students.tiri, students.angkat, students.yatim, students.gol_darah, students.tinggi, students.berat, students.keb_khusus, students.alamat, students.rt, students.rw, students.desa, students.kecamatan, students.kota, students.prov');
        $this->db->select('ayah.nama as ayahNama, ayah.tempat_lahir as ayahTempatLahir, ayah.tgl as ayahTgl, ayah.bln as ayahBln, ayah.thn as ayahThn, ayah.agama as ayahAgama, ayah.kewarg as ayahKewarg, ayah.masih_bersama as ayahMasihBersama, ayah.pendidikan as ayahPendidikan, ayah.pekerjaan as ayahPekerjaan, ayah.penghasilan as ayahPenghasilan, ayah.pengeluaran as ayahPengeluaran, ayah.life as ayahLife, ayah.phone as ayahPhone');
        $this->db->select('ibu.nama as ibuNama, ibu.tempat_lahir as ibuTempatLahir, ibu.tgl as ibuTgl, ibu.bln as ibuBln, ibu.thn as ibuThn, ibu.agama as ibuAgama, ibu.kewarg as ibuKewarg, ibu.masih_bersama as ibuMasihBersama, ibu.pendidikan as ibuPendidikan, ibu.pekerjaan as ibuPekerjaan, ibu.penghasilan as ibuPenghasilan, ibu.pengeluaran as ibuPengeluaran, ibu.life as ibuLife, ibu.phone as ibuPhone');
        $this->db->select('wali.type as waliType, wali.nama as waliNama, wali.tempat_lahir as waliTempatLahir, wali.tgl as waliTgl, wali.bln as waliBln, wali.thn as waliThn, wali.agama as waliAgama, wali.kewarg as waliKewarg, wali.masih_bersama as waliMasihBersama, wali.pendidikan as waliPendidikan, wali.pekerjaan as waliPekerjaan, wali.penghasilan as waliPenghasilan, wali.pengeluaran as waliPengeluaran, wali.life as waliLife, wali.phone as waliPhone');
        $this->db->where('students.id', $id);
        $this->db->join('walis as ayah', 'students.ayah = ayah.id');
        $this->db->join('walis as ibu', 'students.ibu = ibu.id');
        $this->db->join('walis as wali', 'students.wali = wali.id');
        $this->db->from('students');
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->row_array();
    }

}