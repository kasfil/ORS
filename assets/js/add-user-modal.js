let addUser = document.getElementById('addUser');
let modalClose = document.getElementById('modalClose');
let modal = document.getElementById('addUserModal');

addUser.addEventListener('click', () => {
	modal.classList.add('is-active');
});

modalClose.addEventListener('click', () => {
	modal.classList.remove('is-active');
	couponOutput.innerHTML = '';
});